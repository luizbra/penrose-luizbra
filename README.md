# My Penrose Config

This is my personal config for the [penrose](https://github.com/sminez/penrose) tiling window manager library. It
aims to be very minimal, and doesn't contain a status bar.
