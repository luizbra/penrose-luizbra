#!/bin/sh -e
cd "$(dirname "$0")"
cargo --version 1> /dev/null
cargo build --release
sudo cp -v target/release/penrose-luizbra /usr/local/bin/penrose
sudo chmod -v 755 /usr/local/bin/penrose
