#[macro_use]
extern crate penrose;

use penrose::{
    core::{
        config::Config,
        helpers::index_selectors,
        layout::{side_stack, Layout, LayoutConf},
        ring::Selector,
    },
    logging_error_handler,
    xcb::new_xcb_backed_window_manager,
    Backward, Forward, InsertPoint, Less, More, Result,
};

fn main() -> Result<()> {
    let config = Config::default()
        .builder()
        .show_bar(false)
        .workspaces(vec!["1", "2", "3", "4"])
        .layouts(vec![Layout::new(
            "",
            LayoutConf::default(),
            side_stack,
            1,
            0.55,
        )])
        .floating_classes(vec!["rofi", "pinentry-gtk-2"])
        .gap_px(8)
        .border_px(2)
        .focused_border(0xb3b1ad)
        .unfocused_border(0x0a0e14)
        .build()
        .unwrap();

    let key_bindings = gen_keybindings! {
        "M-j" => run_internal!(cycle_client, Forward);
        "M-k" => run_internal!(cycle_client, Backward);
        "M-S-j" => run_internal!(drag_client, Forward);
        "M-S-k" => run_internal!(drag_client, Backward);
        "M-f" => run_internal!(toggle_client_fullscreen, &Selector::Focused);
        "M-q" => run_internal!(kill_client);
        "M-Tab" => run_internal!(toggle_workspace);
        "M-S-l" => run_internal!(update_main_ratio, More);
        "M-S-h" => run_internal!(update_main_ratio, Less);
        "M-S-q" => run_internal!(exit);
        "M-p" => run_external!("rofi -show run");
        "M-Return" => run_external!("alacritty");
        "M-w" => run_external!("librewolf");
        "M-l" => run_external!("slock");
        "M-equal" => run_external!("pulsemixer --change-volume +5");
        "M-minus" => run_external!("pulsemixer --change-volume -5");
        "M-C-q" => run_external!("poweroff");
        "M-C-r" => run_external!("reboot");

        refmap [ config.ws_range() ] in {
            "M-{}" => focus_workspace [ index_selectors(config.workspaces().len()) ];
            "M-S-{}" => client_to_workspace [ index_selectors(config.workspaces().len()) ];
        };
    };

    let mut wm = new_xcb_backed_window_manager(config, vec![], logging_error_handler())?;
    wm.set_client_insert_point(InsertPoint::Last)?; // attachasideandbelow
    wm.grab_keys_and_run(key_bindings, map! {})?;

    Ok(())
}
